package dunaev.mts.service.Schedule;

import dunaev.mts.dao.model.Status;
import dunaev.mts.dao.model.Task;
import dunaev.mts.service.TaskService;
import dunaev.mts.service.TimeService;

public class RunnableTask implements Runnable {

    private TaskService taskService;
    private TimeService timeService;
    private int id;
    private Status status;

    public RunnableTask(TaskService taskService, TimeService timeService, int id, Status status) {
        this.taskService = taskService;
        this.timeService = timeService;
        this.id = id;
        this.status = status;
    }

    @Override
    public void run() {
        Task task = taskService.getTask(id);
        task.setStatus(status)
                .setTime(timeService.getTime());
        taskService.update(task);
    }
}
