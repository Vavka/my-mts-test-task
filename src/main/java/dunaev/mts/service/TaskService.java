package dunaev.mts.service;

import dunaev.mts.dao.model.Task;

public interface TaskService {

    Task createTask();

    Task getTask(int id);

    Task update(Task task);

    void startTask(Task task);
}
