package dunaev.mts.service;

import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;


@Service
public class CustomTimeService implements TimeService {

    private TimeZone timeZone = TimeZone.getTimeZone("Europe/Moscow");
    private DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    @PostConstruct
    private void setTimeZone() {
        dateFormat.setTimeZone(timeZone);
    }

    @Override
    public String getTime() {
        return dateFormat.format(new Date());
    }
}
