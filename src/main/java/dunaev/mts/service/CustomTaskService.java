package dunaev.mts.service;

import dunaev.mts.dao.model.Status;
import dunaev.mts.dao.model.Task;
import dunaev.mts.dao.repository.TaskRepository;
import dunaev.mts.service.Schedule.RunnableTask;
import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
@RequiredArgsConstructor
public class CustomTaskService implements TaskService {

    private final TimeService timeService;
    private final TaskRepository taskRepository;
    private final ThreadPoolTaskScheduler taskScheduler;

    @Override
    public Task createTask() {
        Task task = new Task()
                .setStatus(Status.CREATING)
                .setTime(timeService.getTime());

        return taskRepository.save(task);
    }

    @Override
    public Task getTask(int id) {
        return taskRepository.findById(id);
    }

    @Override
    public Task update(Task task) {
        return taskRepository.save(task);
    }

    @Override
    public void startTask(Task task) {
        task.setStatus(Status.RUNNING)
                .setTime(timeService.getTime());
        task = taskRepository.save(task);
        taskScheduler.schedule(
                new RunnableTask(this, timeService, task.getId(), Status.FINISHED),
                new Date(System.currentTimeMillis() + 120000)
        );

    }
}
