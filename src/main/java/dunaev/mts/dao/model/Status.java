package dunaev.mts.dao.model;

public enum Status {
    CREATING,
    RUNNING,
    FINISHED
}
