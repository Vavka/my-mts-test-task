package dunaev.mts.api.response;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class InfoResponse {
    private String info;
}
