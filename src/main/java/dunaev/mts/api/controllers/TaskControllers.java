package dunaev.mts.api.controllers;

import dunaev.mts.api.response.InfoResponse;
import dunaev.mts.api.response.NewTaskResponse;
import dunaev.mts.api.response.TaskResponse;
import dunaev.mts.dao.model.Task;
import dunaev.mts.service.TaskService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@AllArgsConstructor
public class TaskControllers {

    private final TaskService taskService;

    @PostMapping("/task")
    public @ResponseBody
    ResponseEntity<NewTaskResponse> newTask() {
        Task task = taskService.createTask();
        taskService.startTask(task);

        return ResponseEntity.accepted().body(
                new NewTaskResponse().setId(task.getId())
        );
    }

    @GetMapping("/task/{id}")
    public @ResponseBody
    ResponseEntity<?> getTask(@PathVariable int id) {
        Task task = taskService.getTask(id);
        if (task == null)
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(
                    new InfoResponse().setInfo("Task not found")
            );
        return ResponseEntity.ok(
                new TaskResponse().setStatus(task.getStatus().toString()).setTimestamp(task.getTime())
        );
    }
}
