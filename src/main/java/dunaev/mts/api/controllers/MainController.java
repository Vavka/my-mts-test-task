package dunaev.mts.api.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MainController {
    @RequestMapping("/")
    @ResponseBody
    public String main() {
        return "Hi! Use API /task or /task/id!";
    }
}
